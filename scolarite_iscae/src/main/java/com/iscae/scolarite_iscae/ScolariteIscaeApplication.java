package com.iscae.scolarite_iscae;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScolariteIscaeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScolariteIscaeApplication.class, args);
	}

}
